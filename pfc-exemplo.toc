\select@language {brazil}
\contentsline {chapter}{Resumo}{vii}{chapter*.1}
\contentsline {chapter}{Abstract}{ix}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introdu��o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo Geral}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Objetivos Espec�ficos}{2}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Motiva��o}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Metodologia}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Organiza��o do Texto}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Ferramentas e Linguagens}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Orienta��o a Objetos}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Padr�o de Projeto MVC (\textit {Model}, \textit {View} e \textit {Controller})}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\textit {Models}}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\textit {Views}}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}\textit {Controllers}}{8}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Desenvolvimento \textit {web}}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}HTML}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}\textit {Client-side script}}{9}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}\textit {Javascript}}{9}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}\textit {Server-side scripts}}{10}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}PHP}{10}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}cURL}{11}{subsection.2.3.6}
\contentsline {chapter}{\numberline {3}\textit {Framework Phalcon}}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Classes e m�todos}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}\textit {Phalcon$\delimiter "026E30F $Di$\delimiter "026E30F $FactoryDefault}}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}\textit {Phalcon$\delimiter "026E30F $Mvc$\delimiter "026E30F $Router}}{17}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}\textit {Phalcon$\delimiter "026E30F $Mvc$\delimiter "026E30F $Controller }}{18}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}\textit {Phalcon$\delimiter "026E30F $Mvc$\delimiter "026E30F $Model }}{20}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}\textit {Phalcon$\delimiter "026E30F $Mvc$\delimiter "026E30F $View}}{22}{subsection.3.1.5}
\contentsline {chapter}{\numberline {4}Sistema de gerenciamento de conte�do (SGC)}{25}{chapter.4}
\contentsline {chapter}{\numberline {5}Pluton}{27}{chapter.5}
\contentsline {section}{\numberline {5.1}Descri��o dos Requisitos}{29}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Requisitos Funcionais}{29}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Requisitos N�o Funcionais}{29}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Primeiro Acesso}{30}{section.5.2}
\contentsline {section}{\numberline {5.3}�rea Administrativa}{34}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Configura��es}{36}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Usu�rios}{39}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Posts}{42}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Estat�sticas}{43}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Atualiza��es}{45}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Plugins}{46}{subsection.5.3.6}
\contentsline {section}{\numberline {5.4}\textit {Blog}}{49}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Postagens}{51}{subsection.5.4.1}
\contentsline {chapter}{\numberline {6}Conclus�o}{53}{chapter.6}
\contentsline {chapter}{\numberline {A}Instala��o da Aplica��o}{57}{appendix.A}
\contentsline {chapter}{\numberline {B}Configurando a \textit {Google API}}{63}{appendix.B}
\contentsline {section}{\numberline {B.1}\textit {Google Console}}{63}{section.B.1}
\contentsline {section}{\numberline {B.2}\textit {Google Analytics}}{66}{section.B.2}
\contentsline {section}{\numberline {B.3}Google Adsense}{69}{section.B.3}
\contentsline {chapter}{\numberline {C}\textit {Twitter API}}{73}{appendix.C}
\contentsline {chapter}{\numberline {D}Diagramas}{75}{appendix.D}
\contentsline {section}{\numberline {D.1}Casos de Uso}{75}{section.D.1}
\contentsline {section}{\numberline {D.2}Diagrama de Classe}{76}{section.D.2}
\contentsline {section}{\numberline {D.3}Diagrama de banco de dados}{78}{section.D.3}
