
<?php

namespace Multiple\Backend\Controllers;

class NovoController extends BaseController {

    public function indexAction() {

        $this->session->start();
        //verifica se h� usu�rio ativo
        if ($this->session->get("user_id") != NULL) {
            //busca informa��es do usu�rio ativo
            $vars = $this->getUserLoggedInformation();
            //busca os dados do menu lateral do sistema
            $vars['menus'] = $this->getSideBarMenus();

            //Seu c�digo fonte aqui...

            //seta os valores retornados para exibi��o na view
            $this->view->setVars($vars);
        }
    }
}