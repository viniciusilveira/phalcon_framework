<?php
//Configurando conex�o com um banco de dados MySQL
//Criando a DI (Dependency Injection)
$di = new PhalconnDInFactoryDefault( );
$di->set('db', function(){
	return new Phalcon\Db\Adapter\Pdo\Mysql(array(
		'host' => 'localhost',
		'username' => 'root',
		'password' => '',
		'dbname' => 'tutorial'
	));
});