<?php


//Sem informar qualquer parametro
Users::find();
Users::findFirst();

//Filtrando a consulta
//Retorna todos os usuarios do tipo 1 a partir do d�cimo registro
Users::find(array(
	"conditions" => "type :id:",
    "order" => "date_register DESC",
    "limit" => 10,
    "offset" => 10,
    "bind" => array("type" => 1)
));

//Retorna o usuario com id igual a 1
Users::findFirst(array(
	"conditions" => "id = :id:",
    "bind" => array("id" => 1)
));

//Retorna todos os usu�rios do tipo 1
Users::findByType(1);

//Retorna o primeiro usu�rio do tipo 1
Users::findFirstByType(1);