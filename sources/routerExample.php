<?php

use Phalcon\Mvc\Router;


$router = new Router();


$router->add(
    "/settings",
    array(
        "controller" => "settings",
        "action"     => "index"
    )
);