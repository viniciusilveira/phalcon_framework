<?php

use Phalcon\Mvc\Router;

$router = new Router();

$router->add(
    "/admin/:controller/a/:action/:params",
    array(
        "controller" => 1,
        "action"     => 2,
        "params"     => 3
    )
);