<?php

use Phalcon\Http\Request;

$di = new Phalcon\Di;

$di->set("request", function () {
    return new Request();
});